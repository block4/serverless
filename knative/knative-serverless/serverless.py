import pg8000
import os
import random
import string
from flask import Flask, request, make_response, jsonify
import redis

db = None
redis_db = None
app = Flask(__name__)


class DatabaseConnector:

    def __init__(self, host, port, database, user, password):
        self.conn = pg8000.connect(host=host, port=int(
            port), database=database, user=user, password=password)
        self.cursor = self.conn.cursor()

    def disconnect(self):
        if self.cursor is not None:
            self.cursor.close()
        if self.conn is not None:
            self.conn.close()

    def execute_query(self, query_statement):
        self.cursor.execute(query_statement)
        return self.cursor.fetchall()

    def execute_update(self, update_statement):
        self.cursor.execute(update_statement)
        self.conn.commit()
        return True


def check_sql_result(result):
    if not str(result) == '()':
        if not len(result) > 1:
            return True
    return False


def get_user_by_token(auth_token):
    try:
        user_id = redis_db.get(auth_token).decode('utf-8')
        result = db.execute_query(
            "select \"user\", login_msg from serverless.users where id = '{}'"
            .format(user_id))
        if check_sql_result(result):
            return (result[0][0], result[0][1])
    except AttributeError:
        return None


def login(user, password):
    result = db.execute_query(
        "select id, \"user\" from serverless.users where \"user\"='{}' and password='{}' "
        .format(user, password))
    if check_sql_result(result):
        return (result[0][0], result[0][1])
    return None


def create_token(id, name):
    token = ''.join(random.choice(string.ascii_lowercase + string.digits) for i in range(12))
    redis_db.set(token, id, ex=300)
    return token


@app.route('/')
def hello_world():
    target = os.environ.get('TARGET', 'World')
    return 'Hello {}!\nDefault user: user password: test'.format(target)


@app.route('/login', methods=['GET', 'POST'])
def login_route():
    if 'username' in request.form and 'password' in request.form:
        user_tuple = login(request.form['username'], request.form['password'])
        if user_tuple:
            response = make_response(
                jsonify('OK', 200, {'Content-Type': 'text/plain'}))
            response.set_cookie('authToken', value=create_token(
                *user_tuple), httponly=True, samesite='Strict')
            return response
        return 'autentication fail', 401, {'Content-Type': 'text/plain'}
    return 'Bad Request', 502, {'Content-Type': 'text/plain'}


@app.route('/user_page')
def user_page():
    if request.cookies['authToken']:
        user_tuple = get_user_by_token(request.cookies['authToken'])
        if user_tuple:
            return 'Hello {}!\n {}'.format(*user_tuple)
    return 'Unauthorized', 401, {'Content-Type': 'text/plain'}


if __name__ == "__main__":
    db = DatabaseConnector(os.environ['host'], os.environ['port'],
                           os.environ['database'],
                           os.environ['user'], os.environ['password'])
    redis_db = redis.Redis(
        host=os.environ.get('redis_host', 'redis'), port='6379', db=0)
    app.run(host='0.0.0.0', port=int(os.environ.get('PORT', 8080)))
