#!/bin/bash
kubectl apply -f 01-postgres-secret.yml
kubectl apply -f 02-postgres.yml
kubectl apply -f 03-postgres-service.yml
sleep 10
kubectl apply -f 04-postgres-init-job.yml
kubectl apply -f 05-redis.yml
kubectl apply -f 06-redis-service.yml
sleep 10
kubectl apply -f 07-serverless.yml
sleep 5
kn service describe knative-serverless