create schema serverless;
SET search_path TO serverless;
create table users
(
    id        serial not null
        constraint users_pk
            primary key,
    "user"    text   not null,
    password  text   not null,
    login_msg text   not null
);

alter table users
    owner to postgres;

create unique index users_id_uindex
    on users (id);

create unique index users_user_uindex
    on users ("user");

INSERT INTO serverless.users (id, "user", password, login_msg) VALUES (1, 'user', 'test', 'Hi');